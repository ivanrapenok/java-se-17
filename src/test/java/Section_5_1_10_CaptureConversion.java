import org.junit.jupiter.api.Test;

import javax.lang.model.type.NullType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Section_5_1_10_CaptureConversion {

    static class G< A1 extends Collection< A2 >, A2 > {
        A1 get() {
            return null;
        }
    }

    static void unboundedWildcard( G< ?, Long > param ) {
        unboundedWildcardHelper( param );
    }

    // Helper method created so that the wildcard can be captured through type inference.
    static < S1 extends Collection< Long > > void unboundedWildcardHelper( G< S1, Long > param ) {
    }

    static void upperBoundedWildcard( G< ? extends Runnable, Long > param ) {
        upperBoundedWildcardHelper( param );
    }

    static < S1 extends Collection< Long > & Runnable > void upperBoundedWildcardHelper( G< S1, Long > param ) {
    }

    static void lowerBoundedWildcard( G< ? super List< Long >, Long > param ) {
        lowerBoundedWildcardHelper( param );
    }

    static < S1 extends Collection< Long > > void lowerBoundedWildcardHelper( G< S1, Long > param ) {
    }

    static void lowerBoundedWildcardCaller() {
//        lowerBoundedWildcard( new G< Iterable< Long >, Long >() );
        lowerBoundedWildcard( new G< Collection< Long >, Long >() );
        lowerBoundedWildcard( new G< List< Long >, Long >() );
//        lowerBoundedWildcard( new G< ArrayList< Long >, Long >() );
    }
}
