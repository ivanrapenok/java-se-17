import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Section_5 {
    @Test
    void section_1_3_narrowingPrimitiveConversion() {
        int positiveSmallInt = 0x7f;
        int negativeSmallInt = 0xffff_ff81;
        int positiveInt = 0x0000_0081;
        int negativeInt = 0xffff_ff7f;

        assertEquals( ( int ) Byte.MAX_VALUE, positiveSmallInt, "Validate small int absolute value" );
        assertEquals( positiveSmallInt, -negativeSmallInt, "Small ints absolute values are equal" );

        assertEquals( ( int ) Byte.MAX_VALUE + 2, positiveInt, "Validate int absolute value" );
        assertEquals( positiveInt, -negativeInt, "Ints absolute numbers are equal" );

        assertEquals( ( byte ) -127, ( byte ) positiveInt, "Positive int to byte (narrowing)" );
        assertEquals( ( byte ) 127, ( byte ) negativeInt, "Negative int to byte (narrowing)" );

        assertEquals( ( byte ) 127, ( byte ) positiveSmallInt, "Positive int to byte (without precision loss)" );
        assertEquals( ( byte ) -127, ( byte ) negativeSmallInt, "Negative int to byte (without precision loss)" );

        assertEquals( 5, ( int ) 5.9f, "Float to int" );
        assertEquals( Long.MAX_VALUE, ( long ) Float.POSITIVE_INFINITY, "Huge positive float to long" );
        assertEquals( Integer.MIN_VALUE, ( int ) Double.NEGATIVE_INFINITY, "Huge negative double to int" );

        assertEquals( '\u007f', ( char ) Byte.MAX_VALUE, "Negative byte to char" );
        assertEquals( '\uff80', ( char ) Byte.MIN_VALUE, "Positive byte to char" );
    }

    @Test
    void section_2_assignmentContext() {
        // a boxing conversion followed by a widening reference conversion
        final Number number = 4L;

        // a widening reference conversion followed by an unboxing conversion,
        // then followed by a widening primitive conversion
        final long i = getIntSubtype( 5 );
    }

    < T extends Integer > T getIntSubtype( T arg ) {
        return arg;
    }

    @Test
    void section_2_assignmentContextThrowsClassCastException() {
        List numberList = new ArrayList< Number >();
        numberList.add( 5L );
        List< String > stringList = numberList;
        assertThrows( ClassCastException.class, () -> {
            String string = stringList.get( 0 );
        } );
    }

    private static final int i = 3;
    @Test
    void section_6_numericContext() {
        short s = 5;
        var i = s + s;
        assertTrue( Integer.class.isInstance( s + s ) , "In a numeric arithmetic context or a numeric array context, the promoted type is int" );
        assertTrue( Integer.MAX_VALUE + 2L != Integer.MAX_VALUE + 1 + 1L, "int+int is promoted to int, then int+long is promoted to long+long" );
    }
}
