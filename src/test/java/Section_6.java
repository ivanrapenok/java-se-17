import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Section_6 {
    @Test
    void canonicalNameVsFullyQualifiedName() {
        assertEquals( "C1.I", C1.I.class.getCanonicalName(), "Canonical name C1.I" );
        assertEquals( "C1.I", C2.I.class.getCanonicalName(), "Canonical name C2.I" );

        // Both p.O1.I and p.O2.I are fully qualified names that denote the member class I,
        // but only p.O1.I is its canonical name.
    }
}

class C1 {
    class I {
    }
}

class C2 extends C1 {
}