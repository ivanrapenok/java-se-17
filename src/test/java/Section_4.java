import org.junit.jupiter.api.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings( { "ClassWithoutConstructor", "ClassWithoutLogger", "DesignForExtension" } )
class Section_4 {

    @Test
    void integerOperations() {
        assertEquals( 2, 5 / 2, "Division" );
        assertEquals( 1, 5 % 2, "Modulus - The remainder operator" );

        int i = 0;
        assertEquals( 1, ++i, "Prefix increment" );
        assertEquals( 1, i++, "Postfix increment" );

        assertEquals( 0xFFFF_0000, ~0x0000_FFFF, "The bitwise complement operator (Tilde accent)" );
        assertEquals( -7, ~6, "The bitwise complement operator: (-x)-1" );

        // integer bitwise operators
        assertEquals( 0xF000, 0xF0F0 & 0xFF00, "The bitwise AND of the operand values (Ampersand)" );
        assertEquals( 0x0FF0, 0xF0F0 ^ 0xFF00, "The bitwise exclusive OR of the operand values (Spacing circumflex accent / Caret)" );
        assertEquals( 0xFFF0, 0xF0F0 | 0xFF00, "The bitwise inclusive OR of the operand values (Vertical bar)" );

        assertTrue( Long.class.isInstance( ( long ) i ), "The cast operator - convert from an integral value to a value of any specified numeric type" );
        assertEquals( "1 box", 1 + " box", "The string concatenation operator" );

        assertEquals( 0x8000_0000, 0x7FFF_FFFF + 1, "Numeric overflow" );
        assertEquals( 0x0000_0007_FFFF_FFF0L, 0x7FFF_FFFF * 16L, "Numeric overflow 2 - cast for demonstration" );
    }

    @Test
    void shiftOperators() {
        assertEquals( 0b0000_1000, 0b0000_0001 << 3, "Left shift" );
        assertEquals( 8, 1 << 3, "Left shift - multiply by 2 to the power of 3" );

        assertEquals( 0xFFFF_FFF8, 0xFFFF_FFFF << 3, "Left shift (negative) - hexadecimal" );
        assertEquals( -8, -1 << 3, "Left shift (negative left-hand operand) - decimal" );
        assertEquals( 1, 1 << 0b10_0000, "Left shift (int left-hand operand) - only the 5 lowest-order bits of the right-hand operand are used as the shift distance" );
        assertEquals( 1 << 0x1F, 1 << 0xFFFF_FFFF, "Left shift (int left-hand operand; negative right-hand) - only the 5 lowest-order bits of the right-hand operand are used as the shift distance" );
        assertEquals( 1L << 0x3F, 1L << 0xFFFF_FFFF, "Left shift (long left-hand operand;  negative right-hand) - only the 6 lowest-order bits of the right-hand operand are used as the shift distance" );

        assertEquals( 0b0001, 0b1010 >> 3, "Signed right shift" );
        assertEquals( 1, 10 >> 3, "Signed right shift - divide by 2 to the power of 3" );

        assertEquals( 0xF8FF_FFAF, 0x8FFF_FAFF >> 4, "Signed right shift with negative left-hand operand" );
        assertEquals( 0x07FF_FFFF, 0x7FFF_FFFF >> 4, "Signed right shift with positive left-hand operand" );
        assertEquals( 0x08FF_FFAF, 0x8FFF_FAFF >>> 4, "Unsigned right shift with negative left-hand operand" );
        assertEquals( 0x07FF_FFFF, 0x7FFF_FFFF >>> 4, "Unsigned right shift with positive left-hand operand" );
    }

    @Test
    void floatingPointOperations() {
        assertEquals( 0.1, 4.5 % 2.2, 0.0001, "Modulus (Floating-point) - The remainder operator" );
        assertTrue( Double.class.isInstance( 5.8 ), "Default floating-point variable type" );
    }

    @Test
    void booleanOperators() {
        int a = 0, b = 0;
        assertFalse( ++a == 0 & ++b == 0, "Logical (bitwise) and operator (Non-short-circuit)" );
        assertEquals( 1, a, "Left operand op. is evaluated" );
        assertEquals( 1, b, "Right operand op. is also evaluated despite the left one is false" );

        int aa = 0, bb = 0;
        assertFalse( ++aa == 0 && ++bb == 0, "The conditional-and operator (short-circuit)" );
        assertEquals( 1, aa, "Left operand op. is evaluated" );
        assertEquals( 0, bb, "Right operand op. is not evaluated" );

        assertNotEquals( false && true || true, false && true | true, "The bitwise operations have higher precedence" );
    }

    @Test
    void newInstance() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Constructor< MyStaticClass > mscc = MyStaticClass.class.getDeclaredConstructor( null );
        assertTrue( mscc.newInstance() instanceof MyStaticClass, "Static inner class or any other class" );
        Constructor< MyClass > mcc = MyClass.class.getConstructor( Section_4.class );
        assertTrue( mcc.newInstance( this ) instanceof MyClass, "Inner class - pass an instance of enclosing class" );
    }

    private long x = 10;

    static class MyStaticClass {
        private long y = 5;
    }

    public class MyClass {
        private long y = Section_4.this.x;
    }

    @Test
    void objectWait() throws InterruptedException {
        Object lock = new Object();
        assertThrows( IllegalMonitorStateException.class, () -> lock.wait( 1 ), "Current thread is not owner (Lock is not acquired)" );
        assertDoesNotThrow( () -> {
            synchronized ( lock ) {
                lock.wait( 1 );
            }
        }, "Current thread is an owner" );
    }

    @Test
    void typeParameter() {
        class X< T extends String & Runnable & Callable< String > > {
        }
    }

    @Test
    void wildcard() {
        interface A {
        }
        interface B extends A {
        }
        interface C extends B {
        }

        class G< T > {
        }

        class F {
            static void anUpperBoundB( G< @I ? extends B > x ) {
            }

            static void aLowerBoundB( G< ? super B > x ) {
            }
        }

        F.anUpperBoundB( new G< B >() );
        F.anUpperBoundB( new G< C >() );

        F.aLowerBoundB( new G< B >() );
        F.aLowerBoundB( new G< A >() );
        F.aLowerBoundB( new G< Object >() );
    }

    @Target( ElementType.TYPE_USE )
    @interface I {
    }

    @Test
    void wildcardProducerExtendsConsumerSuper() {
        var numbers = new ArrayList< Number >( 2 );

        var integers = new ArrayList< Integer >( 1 );
        integers.add( 0xCAFE );
        var longs = new ArrayList< Long >( 1 );
        longs.add( 0xBABEL );

        // Producer (? extends E). We ‘read’ from a ‘producer’, and take that stuff into our own box.
        numbers.addAll( integers );
        numbers.addAll( longs );

        // Consumer (? super E). We ‘write’ our own box into a ‘consumer’.
        var objectsComparator = Comparator.comparingInt( Object::hashCode );
        var numberComparator = Comparator.comparingInt( Number::intValue );
        numbers.sort( objectsComparator );
        numbers.sort( numberComparator );

        // Neither producer nor consumer (?)
        var objects = new ArrayList<>( 2 );
        objects.add( numbers.get( 1 ) );
        objects.add( new Object() );
        numbers.removeAll( objects );

        // Producer and consumer (E)
        numbers.replaceAll( Number::intValue );
    }

    @Test
    void printAnyCollectionObjectOrWildcard() {
        class Printer {
            static void printObjectCollection( Collection< Object > objects ) {
                objects.forEach( System.out::println );
            }

            static void printWildcardCollection( Collection< ? > objects ) {
                objects.forEach( System.out::println );
            }
        }

        List< String > strings = new ArrayList<>();
//        Printer.printObjectCollection( strings );
        Printer.printWildcardCollection( strings );
    }

    @Test
    void staticMemberOfGenericClass() {
        class G< T > {
            //            static T st;
            T t;
        }
    }

    @Test
    void section_12_2_heapPollution() {
        /*
        Note that a variable is not guaranteed to always refer to a subtype of its declared type, but only to subclasses
        or subinterfaces of the declared type. This is due to the possibility of heap pollution discussed below.
        */

        List l = new ArrayList< Long >( 3 );
        l.add( 0L );
        l.add( "value-0" );
        List< String > ls = l;
        ls.add( "value-1" );

        assertTrue( !( ls.get( 0 ) instanceof String ) );
    }
}
