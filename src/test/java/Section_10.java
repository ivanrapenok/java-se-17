import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Section_10 {

    @Test
    void section_5_arrayStoreException() {
        // An array creation expression with a non-reifiable element type is forbidden
        // List<Long>[] array = new List<Long>[0];

        class Point {
            int x, y;
        }
        class ColoredPoint extends Point {
            int color;
        }

        Point[] pa = new ColoredPoint[ 10 ];
        assertTrue( pa[ 1 ] == null );
        assertThrows( ArrayStoreException.class, () -> pa[ 0 ] = new Point() );
    }
}
