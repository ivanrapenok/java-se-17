import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Section_15 {
    @Test
    void section_15_unaryOperators() {
        char c = '\u0001';
        assertEquals( 1, +c, "Unary plus operator" );
        assertEquals( -1, -c, "Unary minus operator" );
        assertEquals( 0x11112222, ~0xeeeedddd, "Bitwise complement operator" );
    }
}
