public class ExploreHeapTemporary {

    public static void main( String[] args ) {
        var stuff = new Stuff[4];
        var person1 = new Person( 1, "1", stuff );
        var person2 = new Person( 2, "2", stuff);
    }

    private static void doAction() {
        int id = 23;
        String pName = "Jon";
        Person p = null;
        p = new Person( id, pName, new Stuff[0]);
        Person[] people = new Person[ 2 ];
        people[ 0 ] = p;
        id++;
    }

    record Person(int pid, String name, Stuff[] stuff) {
    }

    record Stuff(int pid) {
    }
}
