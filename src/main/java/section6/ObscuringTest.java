package section6;

import _package._Class;

public class ObscuringTest {
    /**
     * A package name is obscured by a field declaration. So the declaration bellow is illegal:
     * {@code private final int _package = _package._Class.VALUE;}
     */
    private final int _package = _Class.VALUE;

    private static void goWild(final int _package) {
        var v = _Class.VALUE;
    }
}

